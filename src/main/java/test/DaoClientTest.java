package test;

import static org.junit.Assert.fail;

import java.sql.SQLException;

import org.junit.Test;

import DAO.DaoAuthentification;
import DAO.DaoClient;
import Model.Authentification;
import Model.Client;

public class DaoClientTest {

	@Test
	public void nombreClientTest() {
		// On regarde le nombre de client
		DaoClient daoCl = new DaoClient();
		int resultAvant = daoCl.nombreClient();

		// On ajoute un nouveau client dans la BDD
		DaoAuthentification daCreat = new DaoAuthentification();
		Authentification authCreat = new Authentification();
		String login = "vcvttttt";
		String mdp = "mdp";
		authCreat.setLogin(login);
		authCreat.setMdp(mdp);
		authCreat.setDroit_acces(false);
		Client client = new Client();
		client.setId(26);
		client.setNom("bbb");
		client.setPrenom("bbb");
		client.setAdresse("bbb");
		daCreat.insertClient(authCreat, client);

		// On r�cup�re le nouveau nombre de client et on compare les deux
		int resultApres = daoCl.nombreClient();

		if (resultApres - resultAvant != 1)
			fail("nombre client");
	}

	@Test
	public void updateTest() throws ClassNotFoundException, SQLException {
		// Id utilis� sur le client test�
		int id = 47;
		String login = "arbooo";
		String mdp = "achat";

		// On cr�e un client

		Client client1 = new Client();
		client1.setId(id);
		client1.setTelephone("0102030405");
		client1.setNom("anciennom");
		client1.setPrenom("ancienprenom");
		client1.setAdresse("ancienadresse");

		// J'ajoute ce client dans la BDD
		DaoAuthentification daoAut = new DaoAuthentification();
		Authentification authCreat = new Authentification();
		authCreat.setLogin(login);
		authCreat.setMdp(mdp);
		authCreat.setDroit_acces(false);


		daoAut.insertClient(authCreat, client1);

		Authentification auth = null;
		auth = daoAut.selectByIdMdp(login, mdp);


		// On r�cup�re la version du client
		int versionAvant = auth.getClient().getVersion();

		// On cr�e un nouveau client

		Client client2 = new Client();
		client2.setId(id);
		client2.setTelephone("09999999");
		client2.setNom("nouveaunom");
		client2.setPrenom("nouveauprenom");
		client2.setAdresse("nouvelleadresse");

		// On modifie les informations du client avec la methode Updtate
		DaoClient daoCl = new DaoClient();
		daoCl.update(client2, id);
		Authentification auth2 = null;

		auth2 = daoAut.selectByIdMdp(login, mdp);

		// On v�rifie si les informations sont diff�rentes
		if(auth2.getClient().getAdresse().equals(client1.getAdresse()))
			fail("pas de changement d'adresse");
		if(auth2.getClient().getNom().equals(client1.getNom()))
			fail("pas de changement de nom");
		if(auth2.getClient().getPrenom().equals(client1.getPrenom()))
			fail("pas de changement prenom");
		if(auth2.getClient().getTelephone().equals(client1.getTelephone()))
			fail("pas de changement telephone");
		if(auth2.getClient().getVersion() == versionAvant)
			fail("pas de changement version");


	}
}
