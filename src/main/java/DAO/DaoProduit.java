package DAO;

import java.util.List;

import javax.persistence.*;

import Model.Fournisseur;
import Model.Produit;

public class DaoProduit {

	public void Delete(int id) {

		EntityManagerFactory emf = Persistence.createEntityManagerFactory("foodtruck");
		EntityManager em = emf.createEntityManager();
		EntityTransaction tx = em.getTransaction();
		tx.begin();

		Produit p = em.find(Produit.class, id);
		if (p != null)
			em.remove(p);
		tx.commit();

		em.close();
		emf.close();
	}

	public void Insert(Produit p) {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("foodtruck");
		EntityManager em = emf.createEntityManager();
		EntityTransaction tx = em.getTransaction();
		tx.begin();

		em.persist(p);

		tx.commit();

		em.close();
		emf.close();

	}

	public List<Produit> findByTypeProduit(String typeProduit) {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("foodtruck");
		EntityManager em = emf.createEntityManager();
		Query query = em.createQuery("Select produit from Produit produit where produit.type_produit='"+typeProduit+"'");
		List<Produit> list = query.getResultList();

		em.close();
		emf.close();
		return list;

	}

	public List<Produit> findAll() {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("foodtruck");
		EntityManager em = emf.createEntityManager();
		Query query = em.createQuery("Select id from Fournisseur");
		List<Produit> list = query.getResultList();

		em.close();
		emf.close();
		return list;
	}

	public void update(Produit p) {

		EntityManagerFactory emf = Persistence.createEntityManagerFactory("foodtruck");
		EntityManager em = emf.createEntityManager();
		EntityTransaction tx = em.getTransaction();
		tx.begin();

		em.merge(p);
		tx.commit();

		em.close();
		emf.close();

	}

	public Produit FindById(int id) {

		EntityManagerFactory emf = Persistence.createEntityManagerFactory("foodtruck");
		EntityManager em = emf.createEntityManager();

		Produit p = em.find(Produit.class, id);
		em.close();
		emf.close();
		return p;
	}
	public int RecupStockById(int id){
		int stock;
		
		EntityManagerFactory emf =Persistence.createEntityManagerFactory("foodtruck");
		EntityManager em = emf.createEntityManager();
		Produit prod= em.find(Produit.class, id);
	
		stock = prod.getStock();
		
		em.close();
		emf.close();
		return stock;
	}
}
