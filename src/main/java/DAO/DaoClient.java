package DAO;

import java.sql.*;
import java.util.Collection;

import javax.persistence.*;
import Model.Client;



public class DaoClient {

	public int nombreClient(){
		int resultat;
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("foodtruck");
		EntityManager em = emf.createEntityManager();
		Query query = em.createQuery("select client  from  Client client");
		Collection<Client> list= query.getResultList();
		resultat = list.size();

		em.close();
		emf.close();
		return resultat;

	}
	

	public void update(Client client, int id) throws ClassNotFoundException, SQLException {
		

			EntityManagerFactory emf = Persistence.createEntityManagerFactory("foodtruck");
			EntityManager em = emf.createEntityManager();
			EntityTransaction tx = em.getTransaction();
			tx.begin();
			//Client client1= em.find(Client.class, id);
			client.setId(id);
			client = em.merge(client);
			tx.commit();
			em.close();
			emf.close();
		}
	}

