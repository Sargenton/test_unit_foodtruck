package DAO;

import java.sql.*;
import java.util.List;

import javax.persistence.*;



import Model.Authentification;
import Model.Client;



public class DaoAuthentification {

	 public Authentification selectByIdMdp(String login, String mdp) {
			EntityManagerFactory emf = Persistence.createEntityManagerFactory("foodtruck");
			EntityManager em = emf.createEntityManager();
			Query query = em.createQuery("select authentification  from  Authentification authentification where login='"+login+"' and mdp='"+mdp+"'");
			List<Authentification> list= query.getResultList();
			em.close();
			emf.close();
			return list.get(0);
		}

		public Authentification selectById(String login) {
			EntityManagerFactory emf = Persistence.createEntityManagerFactory("foodtruck");
			EntityManager em = emf.createEntityManager();
			Query query = em.createQuery("select authentification  from  Authentification authentification where login='"+login+"'");
			List<Authentification> list= query.getResultList();
			em.close();
			emf.close();
			return list.get(0);
		}

	 public void insertClient(Authentification authentification, Client client)  {
			
			authentification.setClient(client);
			
			EntityManagerFactory emf = Persistence.createEntityManagerFactory("foodtruck");
			EntityManager em = emf.createEntityManager();
			EntityTransaction tx = em.getTransaction();
			tx.begin();
			
			em.persist(client);
			em.persist(authentification);
			
			tx.commit();
			em.close();
			emf.close();

		}
}
