package User;

import Model.*;
import DAO.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Scanner;

public class Menus {


    // Menu général qui permet de s'identifier et rediriger vers un autre menu
    public static void menuAuthentification() {
        // Initialisation du dao , utilisateur et scanner
        DaoAuthentification daoAuthentification = new DaoAuthentification();
        Authentification utilisateur = null;
        Scanner sclog = new Scanner(System.in);
        // Entrée des identifiants via console
        System.out.println("Bienvenue au Food Truck SKFN");
        System.out.println("*****************************");
        System.out.print("Veuillez entrer votre login : ");
        String login = sclog.nextLine();
        System.out.print("Veuillez entrer votre mot de passe : ");
        String pass = sclog.nextLine();
        // On essaye de récupérer dans la BDD l'utilisateur correspondant
        try {
            utilisateur = daoAuthentification.selectByIdMdp(login, pass);
        } catch (Exception e) {
            System.out.println(e);
        }
        // Soit le client n'est en BDD.
        if (utilisateur == null) {
            System.out.println("aucune correspondance trouvée entre login et mot de passe ");
            System.out.println("Voulez vous :");
            System.out.println("1. Reessayer ");
            System.out.println("2. Creer un compte ");
            String choice = sclog.nextLine();
            if (choice.equals("2")) {
                menuCreaCompte();
            }
        }
        // Soit on lance le menu client soit admin
        else {
            if (utilisateur.isDroit_acces())
                menuAdmin(utilisateur);
            else if (utilisateur.isDroit_acces() == false)
                menuClient(utilisateur);
        }
    }

    public static void menuCreaCompte() {
        DaoClient daoClient = new DaoClient();
        DaoAuthentification daoAuthentification = new DaoAuthentification();
        Scanner scCrea = new Scanner(System.in);
        String choice = "";
        Authentification utilisateur = new Authentification();
        Client client = new Client();
        utilisateur.setClient(client);
        client.setAuthentification(utilisateur);
        utilisateur.setDroit_acces(false);
        System.out.println("Saissisez Login :");
        choice = scCrea.nextLine();
        boolean loginExist = true;
        try {
            daoAuthentification.selectById(choice);
        } catch (Exception e) {
            loginExist = false;
        }
        if ( loginExist){
            System.out.println("Le login est deja pris");
        } else{
            utilisateur.setLogin(choice);
            System.out.println("Saissisez mdp :");
            choice = scCrea.nextLine();
            utilisateur.setMdp(choice);
            System.out.println("Saissisez nom :");
            choice = scCrea.nextLine();
            client.setNom(choice);
            System.out.println("Saissisez prénom :");
            choice = scCrea.nextLine();
            client.setPrenom(choice);
            System.out.println("Saissisez adresse :");
            choice = scCrea.nextLine();
            client.setAdresse(choice);
            System.out.println("Saissisez téléphone :");
            choice = scCrea.nextLine();
            client.setTelephone(choice);
            daoAuthentification.insertClient(utilisateur, client);
            System.out.println("Client créer");
            System.out.println(utilisateur);
        }

    }

    public static void menuAdmin(Authentification utilisateur) {
        Scanner scAdmin = new Scanner(System.in);


    }

    public static void menuClient(Authentification utilisateur) {
        Commande commande = new Commande();
        // TODO générer un bon id quand on sauvegardera la commande
        commande.setNumeroCommande(1);
        commande.setAuthentification(utilisateur);
        Formule formule = null;
        Scanner scFormule = new Scanner(System.in);
        boolean continuer = true;
        while (continuer) {
            formule = new Formule();
            System.out.println("Chosissez une formule :");
            System.out.println("1_ Petit déjeuner ( Boisson Chaude + Dessert )");
            System.out.println("2_ Déjeuner ( Entrée + Plat + Dessert + Boisson )");
            System.out.println("3_ Goûter ( Dessert + Boisson )");
            System.out.println("4_ Diner ( Plat + Dessert + Boisson )");
            System.out.println("99_ Annuler Commande");
            System.out.println("100_ Valider Commande");
            String choixFormule = scFormule.nextLine();
            int numFormule = 0;
            try {
                numFormule = Integer.parseInt(choixFormule);
            } catch (Exception e) {
                System.out.println("Ce n'est pas un entier");
            }
            // TODO fichier de configuration pour différente type de formules
            switch (numFormule) {
                case 1:
                    formule.setNomFormule("Petit dejeuner");
                    formule.getListeProduit().add(menuBoissonChaude());
                    formule.getListeProduit().add(menuDessert());
                    if (produitNonNullDansFormule(formule))
                        commande.getListeFormule().add(formule);

                    break;
                case 2:
                    formule.setNomFormule("Dejeuner");
                    formule.getListeProduit().add(menuEntree());
                    formule.getListeProduit().add(menuPlat());
                    formule.getListeProduit().add(menuDessert());
                    formule.getListeProduit().add(menuBoisson());
                    if (produitNonNullDansFormule(formule))
                        commande.getListeFormule().add(formule);
                    break;
                case 3:
                    formule.setNomFormule("Gouter");
                    formule.getListeProduit().add(menuDessert());
                    formule.getListeProduit().add(menuBoisson());
                    if (produitNonNullDansFormule(formule))
                        commande.getListeFormule().add(formule);
                    break;
                case 4:
                    formule.setNomFormule("Diner");
                    formule.getListeProduit().add(menuPlat());
                    formule.getListeProduit().add(menuDessert());
                    formule.getListeProduit().add(menuBoisson());
                    if (produitNonNullDansFormule(formule))
                        commande.getListeFormule().add(formule);
                    break;
                case 99:
                    System.out.println("Commande annulee");
                    //TODO remise en stock SI ANNULE
                    continuer = false;
                    break;
                case 100:
                    System.out.println("Commande validee");

                    continuer = false;
                    break;
                default:
                    System.out.println("Erreur de saisie");
                    break;
            }
            System.out.println("Etat commande");
            System.out.println(commande);
        }

    }

    public static Produit menuBoissonChaude() {
        DaoProduit daoProduit = new DaoProduit();
        Scanner scBC = new Scanner(System.in);
        String choix;
        int nbChoix = 0;
        Collection<Produit> listProduitsALL = daoProduit.findByTypeProduit("boisson chaude");
        ArrayList<Produit> listaffichage = new ArrayList<>();

        for (Produit p :
                listProduitsALL) {
            if (p.getStock() > 0)
                listaffichage.add(p);
        }
        System.out.println("Choissisez une boisson chaude :");
        for (int i = 0; i < listaffichage.size(); i++) {
            System.out.print(i + 1);
            System.out.println("_" + listaffichage.get(i));
        }
        choix = scBC.nextLine();
        try {
            nbChoix = Integer.parseInt(choix);
        } catch (Exception e) {
            System.out.println("Ce n'est pas un entier");
        }
        if (nbChoix != 0 && nbChoix < listaffichage.size() + 1) {
            Produit produitcommande = listaffichage.get(nbChoix - 1);
            produitcommande.setStock(produitcommande.getStock() - 1);
            System.out.println(produitcommande);
            daoProduit.update(produitcommande);
            return produitcommande;

        } else {
            System.out.println("Erreur de saisie");
            return null;
        }
    }

    public static Produit menuBoisson() {
        DaoProduit daoProduit = new DaoProduit();
        Scanner scBC = new Scanner(System.in);
        String choix;
        int nbChoix = 0;

        Collection<Produit> listProduitsALL = daoProduit.findByTypeProduit("boisson");
        ArrayList<Produit> listaffichage = new ArrayList<>();
        for (Produit p :
                listProduitsALL) {
            if (p.getStock() > 0)
                listaffichage.add(p);
        }
        System.out.println("Choissisez une boisson :");
        for (int i = 0; i < listaffichage.size(); i++) {
            System.out.print(i + 1);
            System.out.println("_" + listaffichage.get(i));
        }
        choix = scBC.nextLine();
        try {
            nbChoix = Integer.parseInt(choix);
        } catch (Exception e) {
            System.out.println("Ce n'est pas un entier");
        }
        if (nbChoix != 0 && nbChoix < listaffichage.size() + 1) {
            Produit produitcommande = listaffichage.get(nbChoix - 1);
            produitcommande.setStock(produitcommande.getStock() - 1);
            System.out.println(produitcommande);
            daoProduit.update(produitcommande);
            return produitcommande;

        } else {
            System.out.println("Erreur de saisie");
            return null;
        }
    }

    public static Produit menuEntree() {

        DaoProduit daoProduit = new DaoProduit();
        Scanner scBC = new Scanner(System.in);
        String choix;
        int nbChoix = 0;

        Collection<Produit> listProduitsALL = daoProduit.findByTypeProduit("entree");
        ArrayList<Produit> listaffichage = new ArrayList<>();
        for (Produit p :
                listProduitsALL) {
            if (p.getStock() > 0)
                listaffichage.add(p);
        }
        System.out.println("Choissisez une Entree :");
        for (int i = 0; i < listaffichage.size(); i++) {
            System.out.print(i + 1);
            System.out.println("_" + listaffichage.get(i));
        }
        choix = scBC.nextLine();
        try {
            nbChoix = Integer.parseInt(choix);
        } catch (Exception e) {
            System.out.println("Ce n'est pas un entier");
        }
        if (nbChoix != 0 && nbChoix < listaffichage.size() + 1) {
            Produit produitcommande = listaffichage.get(nbChoix - 1);
            produitcommande.setStock(produitcommande.getStock() - 1);
            System.out.println(produitcommande);
            daoProduit.update(produitcommande);
            return produitcommande;

        } else {
            System.out.println("Erreur de saisie");
            return null;
        }
    }

    public static Produit menuPlat() {
        DaoProduit daoProduit = new DaoProduit();
        Scanner scBC = new Scanner(System.in);
        String choix;
        int nbChoix = 0;
        Collection<Produit> listProduitsALL = daoProduit.findByTypeProduit("plat");
        ArrayList<Produit> listaffichage = new ArrayList<>();
        for (Produit p :
                listProduitsALL) {
            if (p.getStock() > 0)
                listaffichage.add(p);
        }
        System.out.println("Choissisez un Plat :");
        for (int i = 0; i < listaffichage.size(); i++) {
            System.out.print(i + 1);
            System.out.println("_" + listaffichage.get(i));
        }
        choix = scBC.nextLine();
        try {
            nbChoix = Integer.parseInt(choix);
        } catch (Exception e) {
            System.out.println("Ce n'est pas un entier");
        }
        if (nbChoix != 0 && nbChoix < listaffichage.size() + 1) {
            Produit produitcommande = listaffichage.get(nbChoix - 1);
            produitcommande.setStock(produitcommande.getStock() - 1);
            System.out.println(produitcommande);
            daoProduit.update(produitcommande);
            return produitcommande;

        } else {
            System.out.println("Erreur de saisie");
            return null;
        }
    }


    public static Produit menuDessert() {
        DaoProduit daoProduit = new DaoProduit();
        Scanner scBC = new Scanner(System.in);
        String choix;
        int nbChoix = 0;
        Collection<Produit> listProduitsALL = daoProduit.findByTypeProduit("dessert");
        ArrayList<Produit> listaffichage = new ArrayList<>();
        for (Produit p :
                listProduitsALL) {
            if (p.getStock() > 0)
                listaffichage.add(p);
        }
        System.out.println("Choissisez un Dessert :");
        for (int i = 0; i < listaffichage.size(); i++) {
            System.out.print(i + 1);
            System.out.println("_" + listaffichage.get(i));
        }
        choix = scBC.nextLine();
        try {
            nbChoix = Integer.parseInt(choix);
        } catch (Exception e) {
            System.out.println("Ce n'est pas un entier");
        }
        if (nbChoix != 0 && nbChoix < listaffichage.size() + 1) {
            Produit produitcommande = listaffichage.get(nbChoix - 1);
            produitcommande.setStock(produitcommande.getStock() - 1);
            System.out.println(produitcommande);
            daoProduit.update(produitcommande);
            return produitcommande;

        } else {
            System.out.println("Erreur de saisie");
            return null;
        }
    }


    public static boolean produitNonNullDansFormule(Formule formule) {
        boolean resultat = true;
        for (Produit p :
                formule.getListeProduit()) {
            if (p == null) {
                resultat = false;
            }
        }
        if (resultat == false) {
            System.out.println("Produit nul trouvé dans la formule");
            System.out.println("Formule non ajoutée à la commande");
        }
        return resultat;
    }


    public static void main(String[] args) {
        while (true)
            menuAuthentification();


    }

}
