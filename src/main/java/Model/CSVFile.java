package Model;

import java.io.BufferedReader;
import java.io.FileReader;

public class CSVFile {
	public void lire() throws Exception {
		BufferedReader br = new BufferedReader(new FileReader("src/main/fich.csv"));

		String ligne = null;
		Produit prod = new Produit();
		Fournisseur fourn = new Fournisseur();
		prod.setFournisseur(fourn);
		while ((ligne = br.readLine()) != null) {
			// Retourner la ligne dans un tableau
			String[] data = ligne.split(";");
			prod.setId(Integer.parseInt(data[0])); 
			prod.setNom_produit(data[1]);
			prod.setStock(Integer.parseInt(data[2])); 
			prod.getFournisseur().setId(Integer.parseInt(data[3])); 
			prod.setType_produit(data[4]);

			System.out.println(prod.getId());
			System.out.println(prod.getNom_produit());
			System.out.println(prod.getStock());
			System.out.println(prod.getType_produit());
			System.out.println(prod.getFournisseur().getId());
			
			// Afficher le contenu du tableau
//			for (String val : data) {
//				System.out.println(val);
//			}
		}
		br.close();
	}
}
