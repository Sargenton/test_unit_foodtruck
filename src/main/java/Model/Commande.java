package Model;

import java.util.ArrayList;

public class Commande {
    private int numeroCommande;
    private Authentification authentification;
    private ArrayList<Formule> listeFormule;

    public Commande() {
        this.listeFormule = new ArrayList<Formule>();
    }

    public int getNumeroCommande() {
        return numeroCommande;
    }

    public ArrayList<Formule> getListeFormule() {
        return listeFormule;
    }

    public Authentification getAuthentification() {
        return authentification;
    }

    public void setAuthentification(Authentification authentification) {
        this.authentification = authentification;
    }

    public void setNumeroCommande(int numeroCommande) {
        this.numeroCommande = numeroCommande;
    }

    public void setListeFormule(ArrayList<Formule> listeFormule) {
        this.listeFormule = listeFormule;
    }

    @Override
    public String toString() {
        return "Commande{" +
                "numeroCommande=" + numeroCommande +
                ", authentification=" + authentification +
                ", listeFormule=" + listeFormule +
                '}';
    }
}
