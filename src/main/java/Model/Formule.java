package Model;

import java.util.ArrayList;

public class Formule {
  private String nomFormule;
  private ArrayList<Produit> listeProduit;


    public Formule() {
        this.nomFormule = "";
        this.listeProduit = new ArrayList<Produit>();
    }

    public Formule(String nomFormule) {
        this.nomFormule = nomFormule;
        this.listeProduit = new ArrayList<Produit>();
    }

    public String getNomFormule() {
        return nomFormule;
    }

    public ArrayList<Produit> getListeProduit() {
        return listeProduit;
    }

    public void setNomFormule(String nomFormule) {
        this.nomFormule = nomFormule;
    }

    public void setListeProduit(ArrayList<Produit> listeProduit) {
        this.listeProduit = listeProduit;
    }

    @Override
    public String toString() {
        return "Formule{" +
                "nomFormule='" + nomFormule + '\'' +
                ", listeProduit=" + listeProduit +
                '}';
    }
}
